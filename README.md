# Wheel of Fortune Android App

![App Demo](IMG_9022-ezgif.com-video-to-gif-converter.gif)

## Overview

This Android app is a Wheel of Fortune game built using Jetpack Compose and Kotlin. It allows users to spin the wheel and experience smooth animations powered by Jetpack Compose's powerful declarative UI framework.

## Features

- **Spin the Wheel**: Interact with the wheel by spinning it with a simple touch gesture.
- **Animations**: Enjoy a delightful spinning animation along with other visually appealing animations implemented using Jetpack Compose.

## Prerequisites

- Android Studio Arctic Fox (2020.3.1) or later
- Kotlin version: 1.5.0 or later
- Android Gradle Plugin version: 7.0.0 or later
- Target SDK version: 31 or later

## Getting Started

1. Clone the repository:

   ```bash
   git clone https://github.com/your-username/your-repository.git
