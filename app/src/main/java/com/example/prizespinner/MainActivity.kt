package com.example.prizespinner

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.os.VibrationEffect
import android.os.Vibrator
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.*
import androidx.compose.animation.core.*
import androidx.compose.foundation.*
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.GenericShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Rect
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.*
import androidx.compose.ui.graphics.drawscope.drawIntoCanvas
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.*
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.core.graphics.*
import com.example.prizespinner.ui.theme.PrizeSpinnerTheme
import kotlinx.coroutines.delay
import java.lang.Math.random
import kotlin.random.Random

class MainActivity : ComponentActivity() {
    @ExperimentalAnimationApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val showProgress = remember { mutableStateOf(false) }
            val color1 = colorResource(id = R.color.yellow_shadow)
            val color2 = colorResource(id = R.color.pointer_yellow)
            val customRipple = remember { mutableStateOf(color2) }
            val buttonClick = remember { mutableStateOf(false)}
            LaunchedEffect(key1 = showProgress.value) {
                delay(3000L)
                showProgress.value = false
            }

            LaunchedEffect(key1 = buttonClick.value) {
                if(buttonClick.value) {
                    customRipple.value = color1
                    delay(100L)
                    customRipple.value = color2
                }
            }
            PrizeSpinnerTheme {
                val isEnabled = remember { mutableStateOf(true) }
                val showPrize = remember { mutableStateOf(false) }
                val isRotated = remember { mutableStateOf(false) }

                val context = LocalContext.current

                Box(
                    modifier = Modifier.fillMaxSize(),
                    contentAlignment = Alignment.Center
                ) {
                    val angle: Float by animateFloatAsState(
                        targetValue = if (isRotated.value && !showProgress.value) (5 * 360F + 20f) else 0F,
                        animationSpec = tween(
                            durationMillis = 4000, // duration
                            easing = FastOutSlowInEasing
                        ),
                        finishedListener = {
                            isEnabled.value = true
                            showPrize.value = true
                            vibrate(200, context)

                        }
                    )
                    val pieces = prizeMapper()
                    calculate(pieces,PrizeDto(color = colorResource(id = R.color.slice4), amount = 5000, share = 8))
                    Box(
                        contentAlignment = Alignment.TopCenter,
                    ) {
                        Box(
                            modifier = Modifier.padding(top = 16.dp),
                            contentAlignment = Alignment.Center
                        ) {
                            CircleWithShadow(300.dp)
                            PrizeWheel(
                                modifier = Modifier.rotate(angle),
                                pieces = pieces,
                                size = 284.dp,
                                centerCircleSize = 40.dp
                            )
                            FinalPrize(
                                PrizeDto(
                                    amount = 100000,
                                    color = colorResource(id = R.color.slice2),
                                    share = 5
                                ),
                                size = 230.dp,
                                showPrize = showPrize.value
                            )
                        }
                        DownArrow(
                            modifier = Modifier,
                            colorResource(id = R.color.pointer_yellow)
                        )
                    }

                }

                Column(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(16.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    AnimatedVisibility(
                        visible = !showPrize.value,
                        enter = scaleIn(),
                        exit = scaleOut()
                    ) { titleColumn(modifier = Modifier.padding(top = 16.dp)) }

                    AnimatedVisibility(
                        visible = showPrize.value,
                        enter = scaleIn(),
                        exit = scaleOut()
                    ) { finalTitleColumn(modifier = Modifier.padding(top = 16.dp)) }
                    Spacer(modifier = Modifier.weight(1f))

                    AnimatedVisibility(visible = showPrize.value) {
                        Box(
                            modifier = Modifier
                                .fillMaxWidth()
                                .clip(RoundedCornerShape(5.dp))
                                .background(colorResource(id = R.color.tapsi_blue))
                                .clickable {
                                    showPrize.value = false
                                    showProgress.value = false
                                    isRotated.value = false
                                    buttonClick.value = false
                                }
                                .padding(16.dp),
                            contentAlignment = Alignment.Center
                        ) {
                            Text(
                                text = "Got it!",
                                textAlign = TextAlign.Center,
                                color = Color.White
                            )
                        }
                    }

                }
            }
        }
    }
}

@Composable
fun titleColumn(modifier: Modifier) {
    Column(
        modifier = modifier
            .fillMaxWidth()
            .padding(top = 40.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(text = "جایزه‌ت آماده‌ست", fontSize = 28.sp, fontWeight = FontWeight.ExtraBold)
        Text(
            modifier = Modifier.padding(top = 12.dp),
            text = "شانست رو امتحان کن",
            fontWeight = FontWeight.Bold,
            fontSize = 16.sp
        )

    }
}

@Composable
fun finalTitleColumn(modifier: Modifier) {
    Column(
        modifier = modifier
            .fillMaxWidth()
            .padding(top = 40.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = "تبریک تپسی به شما سفیر گرامی",
            fontSize = 20.sp,
            fontWeight = FontWeight.ExtraBold
        )
        Text(
            modifier = Modifier.padding(top = 12.dp),
            text = "در لحظه برای شما واریز می‌شود",
            fontWeight = FontWeight.Bold,
            fontSize = 16.sp
        )

    }
}


fun vibrate(duration: Long,context: Context){
    val vibrator = context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
    if (Build.VERSION.SDK_INT >= 26) {
        vibrator.vibrate(VibrationEffect.createOneShot(duration, VibrationEffect.DEFAULT_AMPLITUDE))
    } else {
        vibrator.vibrate(duration)
    }
}
val prizes : HashMap<Int,Float> = hashMapOf()

@Composable
fun prizeMapper(): List<PrizePieceData> {
    val prizeDtoList =
        listOf(
            PrizeDto(color = colorResource(id = R.color.slice1), amount = 50000, share = 1),
            PrizeDto(color = colorResource(id = R.color.slice2), amount = 20000, share = 4),
            PrizeDto(color = colorResource(id = R.color.slice3), amount = 10000, share = 2),
            PrizeDto(color = colorResource(id = R.color.slice4), amount = 5000, share = 8),
            PrizeDto(color = colorResource(id = R.color.slice4), amount = 2000, share = 16)
        )
    var totalShare = 0
    prizeDtoList.map { totalShare += it.share }
    var remainsAngle = 0f
    val prizeDataList: MutableList<PrizePieceData> = mutableListOf()
    prizeDtoList.map {
        prizeDataList.add(
            PrizePieceData(
                startAngle = remainsAngle,
                arcSweep = (360f * it.share) / totalShare,
                shadowSweep = 3f,
                arcColor = it.color,
                borderWidth = 2.dp,
                arcSize = 250.dp,
                prizeText = it.amount
            )
        )
        prizes[it.amount] = remainsAngle +((360f * it.share) / (totalShare*2))
        remainsAngle += (360f * it.share) / totalShare
    }
    return prizeDataList
}

fun createTriangle(width: Float) = GenericShape { size, _ ->
    moveTo((size.width / 2f) - (width), 0f)
    lineTo((size.width / 2f) + (width), 0f)
    lineTo((size.width / 2f) + (width), (2 * width))
    lineTo((size.width / 2f), 3 * width)
    lineTo((size.width / 2f) - (width), 2 * width)
    lineTo((size.width / 2f) - (width), 0f)
}

fun textContrastColor(background: Color): Int {
    val color = android.graphics.Color.argb(
        background.toArgb().alpha,
        background.toArgb().red,
        background.toArgb().green,
        background.toArgb().blue
    )
    return if (ColorUtils.calculateContrast(android.graphics.Color.BLACK, color) < 4.5) {
        android.graphics.Color.WHITE
    } else {
        android.graphics.Color.BLACK
    }
}

@Composable
fun DownArrow(modifier: Modifier, color: Color) {
    Box(
        modifier = modifier
            .size(width = 36.dp, height = 56.dp)
            .shadow(elevation = 2.dp, createTriangle(with(LocalDensity.current) { 19.dp.toPx() }))
            .clip(createTriangle(with(LocalDensity.current) { 18.dp.toPx() }))
            .background(color)
            .border(2.dp, Color.White, createTriangle(with(LocalDensity.current) { 18.dp.toPx() })),
    )
}

fun Path.moveTo(offset: Offset) = moveTo(offset.x, offset.y)
fun Path.lineTo(offset: Offset) = lineTo(offset.x, offset.y)

fun roundedRectangle(rect: Rect) = Path().apply {
    moveTo(rect.topLeft)
    lineTo(rect.topRight)
    lineTo(rect.bottomRight - Offset(30f, 0f))
    lineTo(rect.bottomLeft + Offset(30f, 0f))
    lineTo(rect.topLeft)
    lineTo(rect.topRight)
}

@Composable
fun RoundedRectShape(
    modifier: Modifier,
    buttonColor: Color,
    shadow: Color,
    content: String,
    showProgress: Boolean
) {
    Box(
        modifier = modifier,
        contentAlignment = Alignment.Center
    ) {
        Canvas(modifier = modifier) {
            val rect = Rect(Offset.Zero, Size(size.width, size.height))
            val rect2 = Rect(Offset.Zero, Size(size.width, size.height - 30f))
            drawIntoCanvas { canvas ->
                canvas.drawOutline(
                    outline = Outline.Generic(roundedRectangle(rect)),
                    paint = Paint().apply {
                        color = shadow
                        pathEffect = PathEffect.cornerPathEffect(rect.maxDimension / 20)

                    }
                )
                canvas.drawOutline(
                    outline = Outline.Generic(roundedRectangle(rect2)),
                    paint = Paint().apply {
                        color = buttonColor
                        pathEffect = PathEffect.cornerPathEffect(rect.maxDimension / 20)

                    }
                )
            }
        }
    }
}




@Composable
fun ContentButton(showProgress: Boolean, text: String){
    Row(
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(
            text = text,
            modifier = Modifier
                .padding(bottom = 8.dp)
        )
        AnimatedVisibility(visible = showProgress) {
            Box(
                modifier = Modifier
                    .padding(start = 8.dp, bottom = 8.dp)
                    .size(18.dp)
            ) {

                CircularProgressIndicator(
                    color = colorResource(id = R.color.yellow_shadow),
                    strokeWidth = 2.dp
                )
            }
        }
    }
}
@ExperimentalAnimationApi
@Composable
fun FinalPrize(prize: PrizeDto, size: Dp, showPrize: Boolean) {
    AnimatedVisibility(
        visible = showPrize,
        enter = scaleIn(),
        exit = scaleOut()
    ) {
        Box(contentAlignment = Alignment.Center) {

            SimpleCircle(
                radius = size,
                color = Color.White,
                borderColor = Color.White,
                borderWidth = 0.dp
            )
            SimpleCircle(
                radius = size - 16.dp,
                color = prize.color,
                borderColor = colorResource(id = R.color.black_shadow),
                borderWidth = 7.dp
            )
            Column(horizontalAlignment = Alignment.End) {
                Text(
                    text = buildAnnotatedString {
                        withStyle(
                            SpanStyle(
                                Color(textContrastColor(prize.color)),
                                fontWeight = FontWeight.ExtraBold,
                                fontSize = 48.sp
                            )
                        ) {
                            append(prize.amount.toPersianDigits(true))
                        }
                    }, textAlign = TextAlign.Center
                )
                Text(
                    text = buildAnnotatedString {
                        withStyle(
                            SpanStyle(
                                Color(textContrastColor(prize.color)),
                                fontWeight = FontWeight.Normal,
                                fontSize = 16.sp
                            )
                        ) {
                            append("تومان")
                        }

                    }, textAlign = TextAlign.End
                )
            }
        }
    }
}

@Composable
fun PrizeWheel(modifier: Modifier, pieces: List<PrizePieceData>, size: Dp, centerCircleSize: Dp) {
    Box(
        modifier = modifier,
        contentAlignment = Alignment.Center,
    ) {
        SimpleCircle(
            radius = size,
            colorResource(id = R.color.dark_blue),
            colorResource(id = R.color.dark_blue),
            0.dp
        )
        pieces.forEach { prizePiece ->
            PrizePiece(
                startAngle = prizePiece.startAngle,
                arcSweep = prizePiece.arcSweep,
                shadowSweep = prizePiece.shadowSweep,
                arcColor = prizePiece.arcColor,
                arcSize = prizePiece.arcSize,
                borderWidth = prizePiece.borderWidth,
                borderColor = prizePiece.borderColor,
                prizeText = prizePiece.prizeText,
                size = size

            )
        }
        CircleWithShadow(radius = centerCircleSize)
    }
}

@Composable
fun CircleWithShadow(radius: Dp) {
    Canvas(modifier = Modifier
        .size(radius)
        .shadow(
            elevation = 8.dp,
            shape = CircleShape,
            clip = true
        ),
        onDraw = {
            drawCircle(color = Color.White)
        })
}

@Composable
fun SimpleCircle(
    radius: Dp,
    color: Color,
    borderColor: Color,
    borderWidth: Dp
) {
    Canvas(modifier = Modifier
        .size(radius)
        .border(width = borderWidth, color = borderColor, shape = CircleShape),
        onDraw = {
            drawCircle(color = color)
        })
}

@Composable
fun ShadowArc(startAngle: Float, sweepAngle: Float, size: Dp) {
    Canvas(modifier = Modifier
        .size(size),
        onDraw = {
            drawArc(
                color = Color.Black,
                startAngle = startAngle,
                sweepAngle = sweepAngle,
                alpha = 0.2f,
                useCenter = true,
            )
        })
}

@Composable
fun PrizeArcs(
    startAngle: Float,
    sweepAngle: Float,
    size: Dp,
    color: Color,
    borderWidth: Dp,
    borderColor: Color,
) {
    Canvas(
        modifier = Modifier
            .size(size)
            .border(width = borderWidth, color = borderColor, shape = CircleShape)
    ) {
        drawArc(
            color = color,
            startAngle = startAngle,
            sweepAngle = sweepAngle,
            useCenter = true,
        )
    }
}


@Composable
fun PrizePiece(
    startAngle: Float,
    arcSweep: Float,
    shadowSweep: Float,
    arcColor: Color,
    arcSize: Dp,
    borderWidth: Dp,
    borderColor: Color,
    prizeText: Int,
    size: Dp
) {
    PrizeArcs(
        startAngle = startAngle,
        sweepAngle = arcSweep,
        size = arcSize,
        color = arcColor,
        borderWidth = borderWidth,
        borderColor = borderColor,
    )
    ShadowArc(
        startAngle = (startAngle + arcSweep - shadowSweep),
        sweepAngle = shadowSweep, size = arcSize
    )
    spannableTxt(
        title = prizeText.toPersianDigits(true),
        currency = "تومان",
        angle = (startAngle + arcSweep / 2) - 180f,
        color = arcColor,
        size = size
    )
}

@Composable
fun spannableTxt(title: String, currency: String, angle: Float, color: Color, size: Dp) {
    Box(
        modifier = Modifier
            .width(size)
            .rotate(angle)
    ) {
        val textColor = Color(textContrastColor(color))
        Text(
            text = buildAnnotatedString {
                withStyle(
                    SpanStyle(
                        textColor,
                        fontWeight = FontWeight.ExtraBold,
                        fontSize = 18.sp
                    )
                ) {
                    append(title)
                }
                append(" ")
                withStyle(
                    SpanStyle(
                        color = textColor,
                        fontWeight = FontWeight.Normal,
                        fontSize = 12.sp
                    )
                ) {
                    append(currency)
                }

            }, modifier = Modifier.padding(32.dp),
            textAlign = TextAlign.End
        )
    }
}

@Preview(showBackground = true)
@Composable
fun WheelOfFortune() {
    ConstraintLayout(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp)
    ) {
        val (spinner, button, arrow, okBtn) = createRefs()
        Box(modifier = Modifier.constrainAs(spinner) {
            top.linkTo(parent.top)
            bottom.linkTo(parent.bottom)
            start.linkTo(parent.start)
            end.linkTo(parent.end)
        }
        ) {
            Box(
                contentAlignment = Alignment.Center,
            ) {
                CircleWithShadow(300.dp)
                val pieces = prizeMapper()
                PrizeWheel(
                    modifier = Modifier, pieces,
                    size = 284.dp,
                    centerCircleSize = 40.dp
                )
            }
        }
        DownArrow(
            modifier = Modifier.constrainAs(arrow) {
                top.linkTo(spinner.top, margin = (-12).dp)
                start.linkTo(spinner.start)
                end.linkTo(spinner.end)
            },
            colorResource(id = R.color.pointer_yellow)
        )
        Box(
            modifier = Modifier.constrainAs(button) {
                top.linkTo(spinner.bottom, margin = 25.dp)
                start.linkTo(parent.start, margin = 64.dp)
                end.linkTo(parent.end, margin = 64.dp)
            },
        ) {
            RoundedRectShape(
                buttonColor = colorResource(id = R.color.pointer_yellow),
                shadow = colorResource(id = R.color.yellow_shadow),
                content = "spin the wheel!",
                showProgress = false,
                modifier = Modifier
                    .height(64.dp)
                    .width(300.dp),
            )
        }
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .clip(RoundedCornerShape(5.dp))
                .background(colorResource(id = R.color.tapsi_blue))
                .constrainAs(okBtn) {
                    bottom.linkTo(parent.bottom, margin = 16.dp)
                    start.linkTo(parent.start, margin = 16.dp)
                    end.linkTo(parent.end, margin = 16.dp)
                }
                .padding(16.dp)
                .clickable { /* Do something */ },
            contentAlignment = Alignment.Center
        ) {
            Text(text = "Got it!", textAlign = TextAlign.Center, color = Color.White)
        }
    }
}

data class PrizeDto(
    val color: Color,
    val amount: Int,
    val share: Int
)

data class PrizePieceData(
    val startAngle: Float,
    val arcSweep: Float,
    val shadowSweep: Float,
    val arcColor: Color,
    val arcSize: Dp,
    val borderWidth: Dp = 2.dp,
    val borderColor: Color = Color.Black,
    val prizeText: Int
)

fun Number.toPersianDigits(shouldSeparate: Boolean = false): String {
    val persianChars = charArrayOf('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹')
    val builder = StringBuilder()
    val str = this.toString()
    for (i in str.indices) {
        if (Character.isDigit(str[i])) {
            builder.append(persianChars[str[i].toInt() - 48])
        } else {
            builder.append(str[i])
        }
    }
    if (shouldSeparate) {
        return insertNumberSeparator(builder.toString())
    }
    return builder.toString()
}
fun calculate(
    magicalWindowPrizes: List<PrizePieceData>,
    finalPrize: PrizeDto,
) {
    magicalWindowPrizes.forEach {
        Log.d("TAG", "calculate: ${it.startAngle} and ${it.arcSweep}")
    }
    val finalAngle =
        magicalWindowPrizes.filter { (it.prizeText == finalPrize.amount) }
     if (finalAngle.isNullOrEmpty()) Log.d("TAG", "result: 0f ")
    else{
        val mf = finalAngle[0].startAngle + (finalAngle[0].arcSweep * Random.nextFloat())
         Log.d("TAG", "result: $mf")
     }

}
private fun insertNumberSeparator(number: String): String {
    var isNegative = false
    var newNumber = number
    if (number.contains('-')) {
        isNegative = true
        newNumber = number.filter { it != '-' }
    }

    val builder = StringBuilder()

    for (i in newNumber.indices) {
        builder.append(newNumber[i])
        if ((newNumber.length - i) % 3 == 1 && i != (newNumber.length - 1)) {
            builder.append(",")
        }
    }

    return if (isNegative) {
        "-$builder"
    } else {
        builder.toString()
    }

}

